function [] = recognizeSingle(letter_path,prepared_dir,SVD)
% letter_path = pot do crke, ki jo zelimo prepoznati
% prepared_dir = pot do shranjenih pripravljenih crk v .mat formatu
% SVD = 1 - uporabi SVD
	if(SVD == 1)
		letter = recognizeLetterSVD(letter_path, prepared_dir);
	else
		letter = recognizeLetter(letter_path, prepared_dir);
	end
	
	letter
endfunction

function RecognizedLetter = recognizeLetterSVD(letter_path,prepared_dir)

	prepared_letters = dir(prepared_dir);
	len = length(prepared_letters);

	% Potrebujemo pripraviti to crko, ki je v .png formatu
	% v stolpcni vektor
	img = imread(letter_path);
	img = rgb2gray(img);
	b = zeros(1,256);
	for j=1:16
		for k=1:16
			b((j-1)*16 + k) = img(j,k);
		end
	end
	b = b';
	
	RecognizedLetter = "";
	minimum_norm = intmax();
	for index=3:len
		folder = prepared_letters(index).folder;
		name = prepared_letters(index).name;

		matrix_path = strcat(folder,"/", name, "/", "Final.mat");
		Ai = load(matrix_path);
		Ai = Ai.A;
	
		[U,S,V] = svd(Ai);
		temp = U * S;
		yi = temp\b;

		res = U' * b - S*yi;
		res = norm(res);

		if(res < minimum_norm)
			RecognizedLetter = name;
			minimum_norm = res;
		end

	end
endfunction	

function RecognizedLetter = recognizeLetter(letter_path,prepared_dir)

	prepared_letters = dir(prepared_dir);
	len = length(prepared_letters);

	img = imread(letter_path);
	img = rgb2gray(img);
	b = zeros(1,256);
	for j=1:16
		for k=1:16
			b((j-1)*16 + k) = img(j,k);
		end
	end
	b = b';
	
	RecognizedLetter = "";
	minimum_norm = intmax();
	for index=3:len
		folder = prepared_letters(index).folder;
		name = prepared_letters(index).name;

		matrix_path = strcat(folder,"/", name, "/", "Final.mat");
		Ai = load(matrix_path);
		Ai = Ai.A;

		xi = pinv(Ai) * b;
		yi = b - Ai*xi;
		yi = norm(yi);
		if(yi < minimum_norm)
			RecognizedLetter = name;
			minimum_norm = yi;
		end
	end
	
endfunction	
