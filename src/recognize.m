function [] = recognize(test_dir,prepared_dir, test_letters)
% source_dir =  direktorij,kjer so shranjeni testni primeri v .png formatu,
%		vsaka crka naj ima svoje testne primere v svojem  direktoriju 
% prepared_dir = direktorij, kjer so shranjene pripravljene crke v  .mat formatu, 
%		 vsaka crka naj ima svoj ucni nabor v svojem direktoriju
% test_letters = katere testne crke testirati, ['a','b','c', ....] ali -1
%		 ce zelimo testirati vse

% Funkcija recognize bo poskusila prepoznati vsako crko v test_dir

% Uporaba : recognize("../dataset/test", "../dataset/prepared", -1)


 [letter_test_dirs] = dir(test_dir);
 len = length(letter_test_dirs);

 for index=3:len
	 test_dir_name = letter_test_dirs(index).name;
	 test_dir_folder = letter_test_dirs(index).folder;
	 test_dir_path = strcat(test_dir_folder, "/", test_dir_name);
	
	 % Gremo skozi vsak direktorij crke
	 low = tolower(test_dir_name);
	 if( test_letters == -1 || strfind(test_letters,low)>=1)
		 [Results,Procent] = recognizeLettersInDir(test_dir_path,test_dir_name, prepared_dir);
		Results 
		Procent
	end
 end

endfunction

function RecognizedLetter = recognizeLetter(letter_path,prepared_dir)
% letter_path = pot do crke, ki jo bomo poskusili prepoznati
% prepared_dir = direktorij, kjer so pripravljene crke v .mat formatu

% Funkcija vrne prepoznano crko

	prepared_letters = dir(prepared_dir);
	len = length(prepared_letters);

	% Potrebujemo pripraviti to crko ki je v .png formatu 
	% v stolpcni vektor
	img = imread(letter_path);
	% Grayscale-amo sliko
	img = rgb2gray(img);
	b = zeros(1,256);
	for j=1:16
		for k=1:16
			b((j-1)*16 + k) = img(j,k);
		end
	end
	b = b';
	
	RecognizedLetter = "";
	minimum_norm = intmax();
	for index=3:len
		folder = prepared_letters(index).folder;
		name = prepared_letters(index).name;

		matrix_path = strcat(folder,"/", name, "/", "Final.mat");
		Ai = load(matrix_path);
		Ai = Ai.A;
		xi = pinv(Ai) * b;

		yi = b - Ai*xi;
		yi = norm(yi);

		if(yi < minimum_norm)
			RecognizedLetter = name;
			minimum_norm = yi;
		end
	end
endfunction	

function [Results,Percentage] = recognizeLettersInDir(dir_path, dir_name, prepared_dir)
% dir_path = direktorij, v katerem so crke, ki jih zelimo prepoznati
% dir_name = ime direktorija, ki je crka (npr. a,b,c,c_,....)
% prepared_dir = direktorij, kjer je ucni nabor crk

% Funkcija poskusi prepoznati vse crke v direktoriju dir_path in izracuna 
% procent pravilno prepoznanih crk

 [letters] = dir(dir_path);
 len = length(letters);

 if(len < 3)
 	 return
 end

 Results = [];

 low1 = tolower(dir_name);
 num = len - 2;
 correct = 0;
 for index=3:len
	% dobimo vsako testno crko
	name = letters(index).name;
	letter_path = strcat(dir_path,"/",name);

	RecognizedLetter = recognizeLetter(letter_path, prepared_dir);
	tmp = tolower(RecognizedLetter);

	if( tmp == low1)
		correct = correct + 1;
	end
	
	Result = [name," : ", RecognizedLetter,"\n"];
	Results = strcat(Results,Result);
 end
 Percentage = (correct/num)*100;

endfunction
