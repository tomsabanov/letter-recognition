function [] = recognizeSVD(test_dir, prepared_dir, k, test_letters)
% source_dir =  direktorij,kjer so shranjeni testni primeri v .png formatu,
%		vsaka crka naj ima svoje testne primere v svojem  direktoriju 
% prepared_dir = direktorij, kjer so shranjene pripravljene crke v  .mat formatu, 
%		 vsaka crka naj ima svoj ucni nabor v svojem direktoriju
% k = stevilo lastnih vrednosti, ki naj si jih zapomnimo, -1 = vse, razpon [0-20]
% 20 je stevilo ucnih primerov vsake crke
% test_letters = katere testne crke testirati, ['a','b','c', ....] ali -1
%		 ce zelimo testirati vse

% Funkcija recognize bo poskusila prepoznati vsako crko v test_dir

% Uporaba : recognizeSVD("../dataset/test", "../dataset/prepared", 20, -1)


 [letter_test_dirs] = dir(test_dir);
 len = length(letter_test_dirs);

 for index=3:len
	 test_dir_name = letter_test_dirs(index).name;
	 test_dir_folder = letter_test_dirs(index).folder;
	 test_dir_path = strcat(test_dir_folder, "/", test_dir_name);
	
	 % Gremo skozi vsak direktorij crke
	 low = tolower(test_dir_name);
	 if( test_letters == -1 || strfind(test_letters,low)>=1)
		 [Results,Procent] = recognizeLettersInDir(test_dir_path,test_dir_name, prepared_dir,k);
		Results 
		Procent
	end
 end

endfunction

function [U,S] =  getK(U,S, k)

	num = k/20;
	
	U_k = num * 256;	
	U_k = floor(U_k);

	cols = columns(U);

	for i=U_k+1:cols
		U(:,i) = 0;
	end

	for i=k+1:columns(S)
		S(:,i) = 0;
	end


endfunction

function RecognizedLetter = recognizeLetter(letter_path,prepared_dir, K)

	prepared_letters = dir(prepared_dir);
	len = length(prepared_letters);

	% potrebujemo pripraviti to crko ki je v .png 
	% spravit jo potrebujemo v en stolpcni vektor
	img = imread(letter_path);
	img = rgb2gray(img);
	b = zeros(1,256);
	for j=1:16
		for k=1:16
			val = img(j,k);
			a1 = 0;
			a2 = 255;
			b1 = -1;
			b2 = 1;
			% [A,B] -> [a,b]
		%	val = b1 + double(val - a1)*double(b2 - b1)/double(a2 - a1);
			b((j-1)*16 + k) = val;
		end
	end
	b = b';
	
	RecognizedLetter = "";
	minimum_norm = intmax();
	for index=3:len
		folder = prepared_letters(index).folder;
		name = prepared_letters(index).name;

		matrix_path = strcat(folder,"/", name, "/", "Final.mat");
		Ai = load(matrix_path);
		Ai = Ai.A;
	
	
		[U,S,V] = svd(Ai);

		if(K >= 0)
			[U,S] = getK(U,S,K);
		end

		temp = U * S;
		yi = temp\b;

		res = U'*b - S*yi;
		res = norm(res);
		if(res < minimum_norm)
			RecognizedLetter = name;
			minimum_norm = res;
		end

	end
endfunction	

function [Results,Percentage] = recognizeLettersInDir(dir_path, dir_name, prepared_dir,K)
% dir_path = direktorij, v katerem so crke, ki jih zelimo prepoznati
% dir_name = ime direktorija, ki je crka (npr. a,b,c,c_,....)
% prepared_dir = direktorij, kjer je ucni nabor crk

% Funkcija poskusi prepoznati vse crke v direktoriju dir_path in izracuna 
% procent pravilno prepoznanih crk

 [letters] = dir(dir_path);
 len = length(letters);

 if(len < 3)
 	 return
 end

 Results = [];

 low1 = tolower(dir_name);
 num = len - 2;
 correct = 0;
 for index=3:len
	% dobimo vsako testno crko
	name = letters(index).name;
	letter_path = strcat(dir_path,"/",name);

	RecognizedLetter = recognizeLetter(letter_path, prepared_dir, K);
	tmp = tolower(RecognizedLetter);

	if( tmp == low1)
		correct = correct + 1;
	end
	
	Result = [name," : ", RecognizedLetter,"\n"];
	Results = strcat(Results,Result);
 end
 Percentage = (correct/num)*100;

endfunction
