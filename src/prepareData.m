function [] = prepareData(source_dir, out_dir)
% source_dir =  direktorij (string), kjer so spravljene vse crke v obliki .png
% out_dir = direktorij, kamor bomo spravljali pripravljene crke (te bodo v obliki .mat)
% za vsako crko bomo shranili vektor te crke v obliki .mat in singularne vrednosti 

% USAGE: prepareData('../dataset/train', '../dataset/prepared')

	[letter_dirs] = dir(source_dir);
	len = length(letter_dirs);
	
	% create out_dir and get full path of out_dir
	mkdir(out_dir);
	[out] = dir(out_dir);
	out_dir = out(2).folder;

	for index=3:len
		dir_name = letter_dirs(index).name;
		dir_folder = letter_dirs(index).folder;
		dir_path = strcat(dir_folder,"/", dir_name);
		prepareLetter(dir_name,dir_path, out_dir);

		% now we have each letter in four files A,U,S,V .mat
		% we have to combine each letter in to a final matrix, where each letter 
		% will represent a column in this final matrix
		buildFinalLetterMatrix(dir_name, out_dir);
	end

endfunction

function [] = prepareLetter(letter_name,letter_dir, out_dir)
 	[files] = dir(letter_dir);
	len = length(files);

	% first create directory in out_dir where we will store .mat files
	mkdir(out_dir, letter_name);
	out_dir = strcat(out_dir, "/", letter_name);

	for index=3:len
		file_name = files(index).name;
		file_folder = files(index).folder;
		file_path = strcat(file_folder,"/", file_name);

		%remove the extension from file_name
		file_name = regexprep(file_name, '\.[^\.]*$', '');
		writeLetterToMat(file_name, file_path, out_dir);
	end

endfunction


function [] = writeLetterToMat(file_name, file_path, out_dir);
	A = zeros(1,256);
	out_A = strcat(out_dir, "/", file_name,".mat");

	  % Preberi sliko iz ucnega nabora
	  img = imread(file_path)(:,:,1);
	  % Za vsako vrstico v sliki
	  for j = 1:16
	    % Za vsak stolpec v sliki
	    for k = 1:16
	      % Vpisi vrednost v vrstico v A,B,...
		val = img(j,k);
		a1 = 0;
		a2 = 255;
		b1 = -1;
		b2 = 1;
		val = b1 + double(val - a1)*double(b2 - b1)/double(a2-a1);
	        A(1, (j-1)*16 + k) = val;
	    end
	  end

	save(out_A, "A");
endfunction


function [] = buildFinalLetterMatrix(dir_name, out_dir)

	out_dir = strcat(out_dir, "/", dir_name);
	out_final = strcat(out_dir,"/", "Final.mat");
	[files] = dir(out_dir);
	len = length(files);

	A_FINAL = zeros(256, 20);
	
	count = 1;
	for index=3:len
		file_name = files(index).name;

		idx = strfind(file_name, "_U");
		if (idx >= 0) 
			continue;
		end

		idx = strfind(file_name, "_S");
		if (idx >= 0) 
			continue;
		end

		idx = strfind(file_name, "_V");
		if (idx >= 0) 
			continue;
		end

		file_path = strcat(out_dir,"/", file_name);

		load(file_path, "A");

		A = reshape(A, 256, 1);
		A_FINAL(:, count) = A;
		count = count + 1;
	end

	A = A_FINAL;
	save(out_final, "A");
end
