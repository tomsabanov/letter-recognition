REZULTATI Z NAVADNIM SISTEMOM (BREZ SVDJA)

Ucni set smo convertali iz 0-255 na interval -1 do +1,
vhodno sliko pa nismo convertali na ta interval, saj je rezultat boljsi 

Kako interpretirati rezultate?

Fotka n_k.png => n predstavlja index crke, 1=a,2=b,3=c, ....k predstavlja index crke v testnem naboru te crke...

Lahka si tudi dataset pogledas, ti bo takoj jasno vse

Crka, ki je desno od " : ", je tista crka ki jo je prepoznal....

Se spodaj pod stolpcem teh prepoznanih crk je pa skalar "Procent", ki pove procentualno
kako pravilno so bile prepoznane crke....




# Created by Octave 5.1.0, Thu May 30 18:05:59 2019 CEST <tom@arch>
# name: Results
# type: sq_string
# elements: 1
# length: 121
1_1.png : a
1_10.png : a
1_2.png : a
1_3.png : a
1_4.png : a
1_5.png : a
1_6.png : a
1_7.png : a
1_8.png : a
1_9.png : a



# name: Procent
# type: scalar
100


# name: Results
# type: sq_string
# elements: 1
# length: 121
2_1.png : b
2_10.png : b
2_2.png : b
2_3.png : b
2_4.png : b
2_5.png : b
2_6.png : i
2_7.png : i
2_8.png : i
2_9.png : i



# name: Procent
# type: scalar
60


# name: Results
# type: sq_string
# elements: 1
# length: 121
3_1.png : i
3_10.png : l
3_2.png : l
3_3.png : l
3_4.png : l
3_5.png : i
3_6.png : i
3_7.png : i
3_8.png : l
3_9.png : l



# name: Procent
# type: scalar
0


# name: Results
# type: sq_string
# elements: 1
# length: 122
4_1.png : i
4_10.png : l
4_2.png : i
4_3.png : c_
4_4.png : i
4_5.png : j
4_6.png : i
4_7.png : l
4_8.png : i
4_9.png : j



# name: Procent
# type: scalar
10


# name: Results
# type: sq_string
# elements: 1
# length: 121
5_1.png : u
5_10.png : i
5_2.png : i
5_3.png : i
5_4.png : i
5_5.png : p
5_6.png : j
5_7.png : p
5_8.png : i
5_9.png : i



# name: Procent
# type: scalar
0


# name: Results
# type: sq_string
# elements: 1
# length: 121
6_1.png : f
6_10.png : f
6_2.png : f
6_3.png : l
6_4.png : l
6_5.png : f
6_6.png : f
6_7.png : f
6_8.png : f
6_9.png : f



# name: Procent
# type: scalar
0


# name: Results
# type: sq_string
# elements: 1
# length: 121
7_1.png : f
7_10.png : f
7_2.png : f
7_3.png : f
7_4.png : f
7_5.png : i
7_6.png : f
7_7.png : f
7_8.png : f
7_9.png : f



# name: Procent
# type: scalar
90


# name: Results
# type: sq_string
# elements: 1
# length: 121
8_1.png : i
8_10.png : g
8_2.png : g
8_3.png : g
8_4.png : g
8_5.png : u
8_6.png : l
8_7.png : g
8_8.png : d
8_9.png : g



# name: Procent
# type: scalar
60


# name: Results
# type: sq_string
# elements: 1
# length: 121
9_1.png : f
9_10.png : h
9_2.png : h
9_3.png : h
9_4.png : h
9_5.png : h
9_6.png : f
9_7.png : a
9_8.png : f
9_9.png : h



# name: Procent
# type: scalar
60


# name: Results
# type: sq_string
# elements: 1
# length: 131
10_1.png : i
10_10.png : i
10_2.png : i
10_3.png : i
10_4.png : i
10_5.png : i
10_6.png : i
10_7.png : i
10_8.png : i
10_9.png : i



# name: Procent
# type: scalar
100


# name: Results
# type: sq_string
# elements: 1
# length: 131
11_1.png : j
11_10.png : j
11_2.png : j
11_3.png : j
11_4.png : j
11_5.png : j
11_6.png : j
11_7.png : j
11_8.png : j
11_9.png : j



# name: Procent
# type: scalar
100


# name: Results
# type: sq_string
# elements: 1
# length: 131
12_1.png : f
12_10.png : l
12_2.png : i
12_3.png : f
12_4.png : i
12_5.png : l
12_6.png : i
12_7.png : k
12_8.png : k
12_9.png : i



# name: Procent
# type: scalar
20


# name: Results
# type: sq_string
# elements: 1
# length: 131
13_1.png : l
13_10.png : l
13_2.png : l
13_3.png : l
13_4.png : l
13_5.png : l
13_6.png : l
13_7.png : l
13_8.png : l
13_9.png : l



# name: Procent
# type: scalar
100


# name: Results
# type: sq_string
# elements: 1
# length: 131
14_1.png : m
14_10.png : m
14_2.png : m
14_3.png : m
14_4.png : m
14_5.png : f
14_6.png : m
14_7.png : m
14_8.png : m
14_9.png : m



# name: Procent
# type: scalar
90


# name: Results
# type: sq_string
# elements: 1
# length: 131
15_1.png : n
15_10.png : n
15_2.png : n
15_3.png : n
15_4.png : u
15_5.png : f
15_6.png : n
15_7.png : u
15_8.png : n
15_9.png : u



# name: Procent
# type: scalar
60


# name: Results
# type: sq_string
# elements: 1
# length: 131
16_1.png : d
16_10.png : o
16_2.png : o
16_3.png : d
16_4.png : d
16_5.png : d
16_6.png : d
16_7.png : d
16_8.png : d
16_9.png : d



# name: Procent
# type: scalar
20


# name: Results
# type: sq_string
# elements: 1
# length: 131
17_1.png : p
17_10.png : p
17_2.png : p
17_3.png : p
17_4.png : p
17_5.png : p
17_6.png : p
17_7.png : p
17_8.png : p
17_9.png : p



# name: Procent
# type: scalar
100


# name: Results
# type: sq_string
# elements: 1
# length: 131
18_1.png : p
18_10.png : f
18_2.png : p
18_3.png : p
18_4.png : p
18_5.png : f
18_6.png : f
18_7.png : p
18_8.png : i
18_9.png : b



# name: Procent
# type: scalar
0


# name: Results
# type: sq_string
# elements: 1
# length: 131
19_1.png : i
19_10.png : i
19_2.png : s
19_3.png : i
19_4.png : i
19_5.png : i
19_6.png : s
19_7.png : i
19_8.png : i
19_9.png : i



# name: Procent
# type: scalar
20


# name: Results
# type: sq_string
# elements: 1
# length: 131
20_1.png : i
20_10.png : i
20_2.png : i
20_3.png : i
20_4.png : i
20_5.png : i
20_6.png : i
20_7.png : i
20_8.png : i
20_9.png : i



# name: Procent
# type: scalar
0


# name: Results
# type: sq_string
# elements: 1
# length: 131
21_1.png : t
21_10.png : t
21_2.png : t
21_3.png : i
21_4.png : t
21_5.png : i
21_6.png : i
21_7.png : t
21_8.png : t
21_9.png : t



# name: Procent
# type: scalar
70


# name: Results
# type: sq_string
# elements: 1
# length: 131
22_1.png : j
22_10.png : j
22_2.png : u
22_3.png : u
22_4.png : j
22_5.png : u
22_6.png : j
22_7.png : u
22_8.png : u
22_9.png : u



# name: Procent
# type: scalar
60


# name: Results
# type: sq_string
# elements: 1
# length: 131
23_1.png : j
23_10.png : i
23_2.png : j
23_3.png : j
23_4.png : v
23_5.png : v
23_6.png : v
23_7.png : i
23_8.png : j
23_9.png : j



# name: Procent
# type: scalar
30


# name: Results
# type: sq_string
# elements: 1
# length: 131
24_1.png : z
24_10.png : z
24_2.png : z
24_3.png : z
24_4.png : z
24_5.png : z
24_6.png : z
24_7.png : z
24_8.png : j
24_9.png : z



# name: Procent
# type: scalar
90


# name: Results
# type: sq_string
# elements: 1
# length: 131
25_1.png : z
25_10.png : z
25_2.png : z
25_3.png : z
25_4.png : i
25_5.png : i
25_6.png : i
25_7.png : i
25_8.png : i
25_9.png : z



# name: Procent
# type: scalar
0


