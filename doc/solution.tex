\documentclass[A4]{article}
\usepackage{amsmath}
\usepackage{commath}
\usepackage[affil-it]{authblk}
\usepackage{listingsutf8}
\lstset{basicstyle=\ttfamily, language=Octave}
\lstset{literate={č}{{\v c}}1 {š}{{\v s}}1 {ž}{{\v z}}1}
\usepackage[pdftex]{graphicx}
\usepackage{wrapfig}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc} 
\usepackage[bottom]{footmisc}
\usepackage{hyperref}

\begin{document}
\title{Hand-written numerals recognition}
\author{Andrej Gorjan, Matevž Eržen, Tom Šabanov}
\affil{ Faculty of Computer and Information Science,\\University of Ljubljana,\\ Slovenia}
\maketitle

\section{Problem introduction}
Handwriting recognition is the ability of a computer to interpret handwitten input given as a photo, touch-screen input etc. to a character or a string of characters. Our primary goal is to be able to recognize upper-case letters of the slovenian alphabet\footnote{Slovenian alphabet: \href{https://en.wikipedia.org/wiki/Slovene_alphabet}{https://en.wikipedia.org/wiki/Slovene\_alphabet}.} given as grayscale pictures ($16 \times 16$ matrices).
\begin{wrapfigure}{r}{0.35\textwidth}
    \centering
    \caption{Input picture}
    \includegraphics[width=0.35\textwidth]{lib/picture_matrix.png}
\end{wrapfigure}

This problem can be tackled in multiple ways, the best of which is probably using machine learning and TensorFlow\footnote{More about TensorFlow technology: \href{https://www.tensorflow.org/learn}{https://www.tensorflow.org/learn}.}. We however won't be using that in favour of a simpler approach.

We will begin with two sets of pictures; a \textit{learning} and a \textit{testing} set. The former will be used to generate one \textit{recognition} matrix for each letter of the alphabet, which we will later use to interpret inputs from the \textit{testing} set.\\
We will recognise which letter is in the input image, by solving systems of linear equations (featuring the input and each of our \textit{recognition} matrices) and then choosing the minimal result.

\section{Implementation}
\subsection{Mathematical interpretation}
Suppose, $K$ denotes a letter from our alphabet. We take a picture of $K$ from the \textit{learning} set and concatenate all columns of $K$'s matrix one under another, to obrain a vertical vector with $256$ elements. We proceed to do so for all pictures of $K$ and combine the vectors we get into a single $256 \times n$ matrix, where $n$ is the number of pictures of the letter $K$ in the \textit{learning} set.
We obtain a matrix $A_K$ which is used to recognise the letter $K$. \\
Now we take an image we would like to recognise from the \textit{testing} set and reshape it in the same way as we did above, to get a vertical vector, which we will denote as $b$.

Let's now examine the system $A_k x = b$. The colums of $A_k$ represent each one of our \textit{learning} images, so we can imagine $A_k$ as being a description of these images.
\begin{align*}
	A_k=
	\begin{bmatrix}
		a_{1,1} & a_{1,2} & \dots & a_{1,n}\\
		a_{2,1} & a_{2,2} & \dots & a_{2,n}\\
		\vdots & \vdots & \ddots & \vdots\\
		a_{256,1} & a_{256,2} & \dots & a_{256,n}\\
	\end{bmatrix}
\end{align*}
The values $a_{1,1}, a_{1,2},...$ represent the first pixel in the upper left corner of each of our $n$ \textit{learning} images, the values $a_{2,1}, a_{2,2},...$ the second and so on, indexed left to right, top to bottom. $b$ is similar, but only contains data from one image, the one we want to recognise.
\begin{align*}
	b=
	\begin{bmatrix}
		b_1\\
		b_2\\
		\vdots \\
		b_{256}
	\end{bmatrix}
\end{align*}
In general this system can not be solved, especially in our case where $n$ will be small and the system will be very overdetermined, therefore we will use the Moore-Penrose inverse\footnote{Moore-Penrose inverse: \href{https://en.wikipedia.org/wiki/Moore-Penrose_inverse}{https://en.wikipedia.org/wiki/Moore-Penrose\_inverse}.}, to find the best approximation for our system: $x_k = A_k^+ b$. Let's think about how this will help us determine which letter is in the image, with an example on the level of individual pixels.
\begin{align*}
	a_{1,1}x_1 + a_{1,2}x_2 + \dots + a_{1,n}x_n = b_1
\end{align*}
If $b_1$ is a large value, and $a_{1,i}$ are large as well, the values $x_{i}$ won't have to be. If, however $a_{1,i}$ are small, then $x_{i}$ will have to be bigger to compensate. \\On the other hand, if $a_{1,i}$ are large and $b_1$ is small, $x_{i}$ will have to be small, but may also be a mix of (large) negative and positive values. Since we are calculating $x$ that satisfies the least squares chriteria, the values will be the best fit for all rows of the matrix.

We suspect that if the values $a_{j,i}$ and $b_j$ are very diffenrent for all $j=1,2,...256$, the components of $x_i$ will be large, and if $a_{j,i}$ and $b_j$ are similar, they will be smaller.

So if we calculate $x_i = A_i^+ b$ for all letters $i \in \{A, B, \cdots ,\text{Ž}\}$, our guess as to which letter was in the \textit{testing} image will be the $i$ where $x_i$ is the smallest, since it will be the best match to the \textit{learning} images for that letter.

\subsection{Equation derivation and implementation}
To reduce the quantity of the data we have to keep, we will split the matrices $A_i$ with singular-value decomposition: $A_i = U_i S_i V_i^T$. Now we will be solving the system $U_i S_i y_i = b$, where $y_i = V^T x_i$. \\\\
The basis of our method are two equalities:
\begin{gather*}
\norm{x_i} = \norm{y_i},\\
\norm{b - A_i x_i} =  \norm{U_i^T b - S_i y_i}.
\end{gather*}
We can easily show both of these are true by using the definition of the SVD\footnote{Singular value decomposition: \href{https://en.wikipedia.org/wiki/Singular_value_decomposition}{https://en.wikipedia.org/wiki/Singular\_value\_decomposition}.}. For $A = U S V^T$, both $U$ and $V$ are defined to be unitary matrices, meaning $U U^T = I$ \& $V V^T = I$, whose columns are orthonormal eigenvectors of $A^TA$ and $AA^T$ respectively.
Since the columns are orthonormal, multiplying the vector $x_i$ with a transpose of $V$ from the left ($V^T x_i$) only rotates $x_i$ in some direction, but doesn't stretch it and it's norm stays the same. $\norm{x_i} = \norm{y_i}$ holds.

The second equality can be proven with the use of the unitary  properties of $U$ and $V$, as well as the definitions of SVD and $y_i$:
\begin{gather*}
U_i  \textbackslash \quad  U_i^T b - S_i y_i \\
= U_i U_i^T b - U_i S_i y_i \\
= b- U_i S_i y_i\\
= b - U_i S_i V_i^T x_i\\
= b- A_i x_i
\end{gather*}
Since both sides of the equation are the same, they also have the same norm - equality is proven. This result alows us to store only $U_i$ and $S_i$ instead of the whole matrix $A_i$. In adition to this, we may also only store the first $k$ singular values\footnote{Singular value: \href{https://en.wikipedia.org/wiki/Singular_value}{https://en.wikipedia.org/wiki/Singular\_value}.} and vectors. This could potentially make the calculations faster, but at the expense of the accuracy of our predictions. How small can $k$ be and still guarantee a certain (say 90\%) accuracy remains to be determined.

\section{Result evaluation}
\subsection{Implementation problems}
All along the way we were experiencing various problems - some we were able to fix, others not. \\
The first problem we encountered was finding the suitable size for our \textit{learning set}. If the set was too small, the accuracy suffered.  However, if the set was too big, recognition of a single letter took over a minute. Because we started with a small set of just 20 images per each letter, we tried to expand it by applying various rotations on each image. This has proven to be ineffective, as the improvement in the accuracy was not significant.\\
Because we grayscaled our images, the values of the individual pixels ranged from 0 to 255. This consequented very large values of norms of matrices. As a resoult of that we decided to transform each pixel in our \textit{learning set} 
to a value in the interval [-1,1], which has helped increase the accuracy.\\
Finally, the problem we couldn't solve was the increase in accuracy if we kept two eigenvectors in U for each eigenvalue in S we chose to keep. Somehow the accuracy got marginally better even though, to our understanding, keeping more eigenvectors than eigenvalues should not have made any difference at all.


\subsection{Final results}
In the end, we settled on 40 \textit{learning pictures} and 10 \textit{testing pictures} per each letter. This gave us the following results. \\
\begin{align}
\begin{tabular}{ |p{1.5cm}||p{1.5cm}|p{1.5cm}|p{1.5cm}|p{1.5cm}|  }
 \hline
 &\multicolumn{4}{|c|}{Number of singular values used} \\
 \hline
 Letter& 40 & 30 & 20 & 10  \\
 \hline
 A & 100 & 100 & 90 & 20\\
 B & 60 & 50 & 20 & 0\\
 C & 0 & 0 & 0 & 40\\
 Č & 10 & 0 & 0 & 0\\
 D & 0 & 0 & 0 & 0\\
 E & 0 & 0 & 0 & 0\\
 F & 90 & 90 & 80 & 10\\
 G & 60 & 0 & 10 & 0\\
 H & 60 & 50 & 70 & 30\\
 I & 100 & 100 & 100 & 100\\
 J & 100 & 80 & 80 & 50\\
 K & 20 & 20 & 10 & 0\\
 L & 100 & 90 & 100 & 90\\
 M & 90 & 60 & 70 & 10\\
 N & 60 & 10 & 60 & 20\\
 O & 20 & 40 & 20 & 0\\
 P & 100 & 100 & 30 & 0\\
 R & 0 & 0 & 0 & 0\\
 S & 20 & 20 & 20 & 70\\
 Š & 0 & 0 & 0 & 0\\
 T & 70 & 60 & 20 & 20\\
 U & 60 & 80 & 30 & 70\\
 V & 30 & 70 & 50 & 90\\
 Z & 90 & 90 & 50 & 0\\
 Ž & 0 & 0 & 0 & 0\\
 \hline
\hline
 Accuracy & 49.6\% & 44.4\% & 36.4\% & 24.8\%\\
 \hline
\end{tabular}
\end{align}
\\
\\
Given that simply guessing which letter is in the image would have a sucess rate of $4\%$, we consider almost $50\%$ to be a good result. We had the most problems with letters that don't differ vastly and are therefore harder to distinguish. This is most obvious for letters as S and Š, C and Č, U and V and so on. Letters were also extremely likely to be mistaken for an I.

One of the goals of this project was also to find out how much we can reduce the size of the data we store, by using a smaller number of singular values and vectors. The highest accuracy of $49.6\%$ was obtained while using all 40 values. When we reduced the number the accuracy quickly lowered. The most segnificant drops were when we started using less than half of our initial regularvalues.

\subsection{Possible optimizations}
As we started testing our algorithm with different inputs, we quickly discovered direct corelation between accuracy and learning set size. In the end we used \textit{only} 40 different learning pictures for each letter, but we are certain, that if we expanded this learning set, our resoults would be even better.

In addition to previous point, we could improve general accuracy of prediction by using more \textit{diverse} learning sets - containing letters drawn by many different people, not only one or two.

The last and possibly most important improvement in the long run, would be distinct \textit{weighting} of deviations from black to white and white to black. In our oppinion this is important (as we discovered while testing) because black dots on place of whites play more important role, than other way around.

\end{document}
